#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>


struct result {
	int begin;// początek przedzialu
	int end; // koniec przedzialu
	int count; // Ile liczb w przedziale
};

int isPrimary(int n)
{ 
	int i,j=0;

	if(n <= 1) return 0;

	for(i=2;i*i<=n;i++) {
		if(n%i == 0) 
			return(0);	
	}
	return(1);
}


//returned in milliseconds
double getTime() {
	struct timeval  tv;
	gettimeofday(&tv, NULL);

	return (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; 
}

int main(int argc, char* argv[]){

	double start = getTime();

	if(argc < 4){
		fprintf(stderr, "Za malo argumentow\n");
		printf("Podaj argumenty w postaci: [liczba procesów] [dolna granica] [gorna granica]\n");
		exit(1);
	}

	int pid;
	int procLimit = atoi(argv[1]);
	int lowerLimit = atoi(argv[2]);
	int upperLimit = atoi(argv[3]);


	//printf("Wyszukiwanie liczb pierwszych w przedziale [%d, %d] wykorzystujac %d procesow\n",
	// lowerLimit, upperLimit, procLimit); 

	int perProc = (upperLimit-lowerLimit)/procLimit;

	int localLowerLimit = lowerLimit;


	int pipeFD[2];
	pipe(pipeFD);

	//argv[0] - run directory
	//argv[1] - parent steps
	for(int i=0;i<procLimit;++i){

		int l = localLowerLimit;
		int u; 

		if(i == procLimit-1){
			u = upperLimit; 
		}else{
			u = localLowerLimit + perProc;
		}

		//printf("Uruchamiania procesu potomnego w przedziale: [%d, %d]\n",l,u);

		//child
		if((pid = fork()) == 0){

			close(pipeFD[0]);
			
			int primaries = 0;
			for(int i=l;i<=u;++i){
				primaries += isPrimary(i);
			}
			struct result res;
			res.begin = l;
			res.end = u;
			res.count = primaries;
			write(pipeFD[1], &res, sizeof(res));						
			close(pipeFD[1]);
			exit(0);
		}

		localLowerLimit += perProc+1;
	}

	//parent
	close(pipeFD[1]);
	int primaries = 0;
	for(int i=0;i<procLimit;++i){
		int status;
		pid = wait(&status);

		struct result res;
		read(pipeFD[0], &res, sizeof(res));
		primaries += res.count;
		printf("Proces (pid: %d) zakonczony. [%d, %d] zawiera %d l. pierwszych\n", pid, res.begin, res.end, res.count); 
	}


	double end = getTime();

	printf("Liczb pierwszych w przedziale [%d, %d] jest %d.\n Odnalezionow w czasie: %f, przez %d procesow\n",
	 lowerLimit, upperLimit, primaries, (end-start)/1000, procLimit); 


	return 0;
} 
