#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

struct result {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
 int count; // Ile liczb w przedziale
};

struct input {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
};


int isPrimary(int n)
{ 
	int i,j=0;

	if(n <= 1) return 0;

	for(i=2;i*i<=n;i++) {
		if(n%i == 0) 
			return(0);	
	}
	return(1);
}

int fileopen(const char * path){
	int file;
	//int stat = mkfifo(path,0666);
	//file exists error code is 17
	/*if(stat < 0 && errno != 17) {
		perror("mkfifo");		
		return -1;
	}*/

	file = open(path,O_RDWR);
	if(file < 0) {
		perror("Open"); 
		return -1;
	}

	return file;
}

int load(int file, int* lower, int* upper){
	struct input in;

	int bytes = read(file, &in, sizeof(in));
	if(bytes < 0){
		perror("read");
		return -1;	
	}

	*lower = in.begin;
	*upper = in.end; 
	return 0;

}


int main(int argc, char*argv[]){

	int fileIn = fileopen("FIFOinput");
	int fileOut = fileopen("FIFOoutput");	
	if(fileIn < 0) exit(1);
	if(fileOut < 0) exit(1);
	
	int lowerLimit;
	int upperLimit;
	load(fileIn, &lowerLimit, &upperLimit);	

	printf("findPrimary(%s): Start\n", argv[0]);
	

	while(lowerLimit != -1 && upperLimit != -1){
		
		int primaries = 0;
		for(int i=lowerLimit;i<=upperLimit;++i){
			primaries += isPrimary(i);
		}

		struct result r;
		r.begin = lowerLimit;
		r.end = upperLimit;
		r.count = primaries;
		int writtencount = write(fileOut, &r, sizeof(r));
		if(writtencount < 0){
			perror("write");
			exit(1);		
		}
		printf("findPrimary(%s): odnaleziono: %d w przedziale [%d,%d]. Zapisano %d bytow\n",argv[0], primaries, lowerLimit, upperLimit, writtencount);
		load(fileIn, &lowerLimit, &upperLimit);	
	}

	printf("findPrimary(%s): Zakonczono\n", argv[0]);
	
	close(fileIn);
	close(fileOut);
	exit(0);
}

