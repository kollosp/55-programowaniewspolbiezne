#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

struct result {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
 int count; // Ile liczb w przedziale
};


int isPrimary(int n)
{ 
	int i,j=0;

	if(n <= 1) return 0;

	for(i=2;i*i<=n;i++) {
		if(n%i == 0) 
			return(0);	
	}
	return(1);
}

int fileopen(){
	int file;
	int stat = mkfifo("FIFO",0666);
	//file exists error code is 17
	if(stat < 0 && errno != 17) {
		perror("mkfifo");		
		return -1;
	}

	file = open("FIFO",O_RDWR);
	if(file < 0) {
		perror("Open"); 
		return -1;
	}

	return file;
}


int main(int argc, char*argv[]){

	if(argc < 3){
		fprintf(stderr, "Za malo argumentow\n");
		
		for(int i=0;i<argc;++i){
			printf("%d. %s\n",i, argv[i]);
		}

		printf("findPrimary: Wprowadz argumenty w postaci: [Granica dolna] [Granica górna]\n");

		exit(0);
	}

	int lowerLimit = atoi(argv[1]);
	int upperLimit = atoi(argv[2]);



	//printf("findPrimary: [%d, %d]\n", lowerLimit, upperLimit);

	if(lowerLimit >= upperLimit){
		fprintf(stderr, "Dolna granica wieksza od gornej\n");
		exit(0);
	}

	int primaries = 0;
	for(int i=lowerLimit;i<=upperLimit;++i){
		primaries += isPrimary(i);
	}

	struct result r;
	r.begin = lowerLimit;
	r.end = upperLimit;
	r.count = primaries;

	int file = fileopen();
	if(file == -1) exit(1);	
		
	int writtencount = write(file, &r, sizeof(r));
	
	if(writtencount < 0){
		perror("write");
		exit(1);		
	}

	close(file);

	printf("findPrimary: odnaleziono: %d w przedziale [%d,%d]. Zapisano %d bytow\n",primaries, lowerLimit, upperLimit, writtencount);
	exit(0);
}

