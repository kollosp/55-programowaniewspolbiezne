#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>

struct result {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
 int count; // Ile liczb w przedziale
};

struct input {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
};

//compile: gcc procm4.cpp && gcc proc_pot.cpp -o proc_pot && ./a.out 1 1 5 1 

//returned in milliseconds
double getTime() {
	struct timeval  tv;
	gettimeofday(&tv, NULL);

	return (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; 
}


int fileopen(const char * path){
	int file;
	int stat = mkfifo(path,0666);
	//file exists error code is 17
	if(stat < 0 && errno != 17) {
		perror("mkfifo");		
		return -1;
	}

	file = open(path,O_RDWR);
	if(file < 0) {
		perror("Open"); 
		return -1;
	}

	return file;
}

void createProcess(int procLimit, int upperLimit, int lowerLimit, int sectionLimit) {
	unlink("FIFOinput");
	int file = fileopen("FIFOinput");
	int pid;
	for(int i=0;i<procLimit;++i){
		if((pid = fork()) == 0){
			char s[5];
			sprintf(s, "%d", i);

			execl("./findPrimary", s,"","", NULL); 
			exit(0);
		}	
	}

	int perSection = (upperLimit - lowerLimit) / sectionLimit;
	for(int i=0;i<sectionLimit;++i){
		struct input in;
		in.begin = lowerLimit;
		
		if(i == sectionLimit-1) in.end = upperLimit;
		else in.end = lowerLimit + perSection;

		write(file, &in, sizeof(in));

		lowerLimit += perSection;	
	}

	struct input in;
	in.end = -1;
	in.begin = -1;
	for(int i=0;i<procLimit;++i){
		write(file, &in, sizeof(in));
	}

	for(int i=0;i<procLimit;++i){
		int status;
		pid = wait(&status);
		printf("Proces (pid: %d) zakonczony %d\n", pid, status); 
	}
	
	close(file);
}

int readProcess(int fileOut, int sectionLimit){
	int primaries = 0;
	for(int i=0;i<sectionLimit;++i){
		struct result res;
		read(fileOut, &res, sizeof(res));
		printf("Odczytano wynik posredni. Liczb pierwszych w przedziale [%d, %d] jest %d\n", res.begin, res.end, res.count); 
		primaries += res.count;	
	}

	close(fileOut);
	return primaries;
}

int main(int argc, char* argv[]){

	double start = getTime();

	if(argc < 5){
		fprintf(stderr, "Za malo argumentow\n");
		printf("Podaj argumenty w postaci: [liczba procesów] [liczba sekcji] [dolna granica] [gorna granica]\n");
		exit(1);
	}

	int pid;
	int procLimit = atoi(argv[1]);
	int sectionLimit = atoi(argv[2]);
	int lowerLimit = atoi(argv[3]);
	int upperLimit = atoi(argv[4]);

	printf("Wyszukiwanie liczb pierwszych w przedziale [%d, %d] wykorzystujac %d procesow, dzielac przedzial na %d podrzedzialow\n",
	 lowerLimit, upperLimit, procLimit, sectionLimit); 

	int localLowerLimit = lowerLimit;

	unlink("FIFOoutput");
	int fileOut = fileopen("FIFOoutput");

	if((pid = fork()) > 0){
		createProcess(procLimit, upperLimit, lowerLimit, sectionLimit);
		
	}else{
		int primaries = readProcess(fileOut, sectionLimit); 
		double end = getTime();
		printf("Liczb pierwszych w przedziale [%d, %d] jest %d.\n Odnalezionow w czasie: %f\n",
			lowerLimit, upperLimit, primaries, (end-start)/1000);

	}

	return 0;
} 
