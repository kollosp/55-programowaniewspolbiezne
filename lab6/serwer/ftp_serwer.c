// Proces odbierajacy komunikaty - wysyla udp_cli
// Wspolpracuje z udp_cli
// Kompilacja gcc udp_serw.c -o udp_serw -lrt
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#define BUFLEN 80
#define KROKI 10
#define PORT 9950

#define OPENR 1 // Otwarcie pliku do odczytu
#define OPENW 2 // Otwarcie pliku do zapisu
#define READ 3 // Odczyt fragmentu pliku
#define CLOSE 4 // Zamkniecie pliku
#define WRITE 5 // Zapis fragmentu pliku
#define OPENDIR 6 // Otworz zdalny katalog
#define READDIR 7 // Czytaj zdalny katalog
#define STOP 10 // Zatrzymanie serwera

#define SIZE 512 // bajtów.
typedef struct {
	int typ; // typ zlecenia
	int ile; // liczba bajtow
	int fh; // uchwyt pliku
	char buf[SIZE]; // bufor
} mms_t;

void blad(char *s) {
	perror(s);
	_exit(1);
}

DIR *d;
struct dirent *dir;

int main(void) {
	mms_t msg;
	struct sockaddr_in adr_moj, adr_cli;
	int s,rec,snd,blen=sizeof(msg),slen;
	// Utworzenie i rejestracja nazwy -----------------
	s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
	if(s < 0) {
		perror("socket");
	}

	// Ustalenie adresu gniazdka serwera
	memset((char *) &adr_moj, 0, sizeof(adr_moj));
	adr_moj.sin_family = AF_INET;
	adr_moj.sin_port = htons(PORT);
	adr_moj.sin_addr.s_addr = htonl(INADDR_ANY);
	
	if (bind(s, &adr_moj, sizeof(adr_moj))==-1) {
		perror("bind");
	}

	
	
	do {
		rec = recvfrom(s, &msg, blen, 0, &adr_cli, &slen);
		
		if(rec < 0) {
			blad("recvfrom()");
		}
	
		printf("Odebrano komunikat %d\n", msg.typ);
		
		switch (msg.typ) {
		case OPENR: 
			msg.fh = open(msg.buf,O_RDONLY);
		break;
		case READ:
			msg.ile = read(msg.fh,msg.buf,SIZE);
		break;
		case CLOSE: 
			close(msg.fh);
		break;

		case OPENW: 
			msg.fh = open(msg.buf,O_RDWR | O_CREAT, 0666);
		break;
		case WRITE: 
			msg.ile = write(msg.fh,msg.buf,msg.ile);
		break;
		case READDIR:
			d = opendir(msg.buf);
			if (d) {
				while ((dir = readdir(d)) != NULL) {
					sprintf(msg.buf, "%s", dir->d_name);
					msg.ile = 1;
					snd = sendto(s, &msg, blen, 0, &adr_cli, slen);
					if(snd < 0){
						perror("sendto()");
					}
 				}
				closedir(d);
			}

			msg.ile = 0;
			
			if(snd < 0){
				perror("sendto()");
			}
		break;
		}

		snd = sendto(s, &msg, blen, 0, &adr_cli, slen);
		
		
		if(snd < 0) {
			perror("sendto");
		}
	} while(msg.typ != STOP);

	close(s);
	return 0;
}
