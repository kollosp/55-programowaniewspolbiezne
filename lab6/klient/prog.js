//instalation instruction
//https://scribles.net/setting-up-bluetooth-serial-port-profile-on-raspberry-pi/

// bt on startup
// https://scribles.net/auto-power-on-bluetooth-adapter-on-boot-up/

//useful:
// sdptool, rfcomm,
// https://scribles.net/setting-up-bluetooth-serial-port-profile-on-raspberry-pi/
// <edit bluetooth service file>
// sudo systemctl daemon-reload
// sudo systemctl restart bluetooth
//
//useful: 
// sdptool, rfcomm,

//run:
// sudo rfcomm watch hci0
// then pair devices (android and desktop) and connect
// open bluetooth serial terminal application

// remembered networks
// /etc/wpa_supplicant/wpa_supplicant.conf
const { spawn, exec } = require('child_process');
let server = new(require('bluetooth-serial-port')).BluetoothSerialPortServer();
let wifi = require('./wifi')
var Gpio = require('onoff').Gpio;

var leds = [new Gpio(17, 'out'), new Gpio(27, 'out'), new Gpio(22, 'out')]

let CHANNEL = 1; // My service channel. Defaults to 1 if omitted.
let UUID = '38e851bc-7144-44b4-9cd8-80549c6f2912'; // My own service UUID. Defaults to '1101' if omitted

console.log("Bluetooth echo server started...")

//let rfcomm = spawn('rfcomm', ['watch', 'hci0'])

//let started = 0
// rfcomm.stdout.on('data', (data) => {
//     console.log(`stdout: ${data}`);
// });

const mainMenu =
    "----System telemetrii elmecosolar----\n" +
    "0. Dostepne opcje menu\n" +
    "1. Ustawienia WiFi\n" +
    "2. Test Ledow\n" +
    "3. Terminal\n" +
    "9. Reboot\n"
const wifiMenu =
    "---- Ustawienia WiFi----\n" +
    "0. Wyswielt dostepne opcje menu\n" +
    "1. Dostepne sieci (skanowanie)\n" +
    "2. Dostepne sieci (zapisane)\n" +
    "3. Aktualne polacznie\n" +
    "4. Polacz\n" +
    "5. Ping\n" +
    "6. Dostepne adresy ip\n" +
    "9. Powrot\n"

setTimeout(() => {
    console.log("listening...")
    serverOn()
},1000)


const testLed = () => {
	for(let l of leds) {
		l.writeSync(1)
	}

	setTimeout(() => {
		for(let l of leds) {
			l.writeSync(0)
		}
	}, 1000)
}

const asyncCommand = function (command) {
    return new Promise((resolve, reject) => {
        exec(command, (err, stdout, stderr) => {
            if (err) {
            //some err occurred
            reject(err)
            } else {
                // the *entire* stdout and stderr (buffered)

                if(stdout) {
                    stdout = stdout.trim()
                    resolve(stdout);
                }else if(stderr){
                    reject(stderr);
                }else resolve("")
            }
        });
    })
}

const showIp =  function() {
    return new Promise((resolve, reject) => {
        exec('hostname -I', (err, stdout, stderr) => {
            if (err) {
                //some err occurred
                reject(err)
            } else {
                // the *entire* stdout and stderr (buffered)

                if(stdout) {
                    stdout = stdout.trim()
                    stdout = "Dostepne interfacy: \n - " + stdout.split(" ").join("\n - ") + "\n"
                    resolve(stdout);
                }else if(stderr){
                    reject(stderr);
                }else resolve("")
            }
        });
    })
}



const serverOn = function(){
    let pwd = 0 //pwd determinating where the user is in the menu.
    let tmpObj = {} //object used to temporarily store data
    // 0 - main menu
    // 1 - wifi settings
    // 2 - connecting to the network
    // 3 - ping
    // 4 - terminal

    const printMenu = function(){
        switch (pwd) {
            case 0: return mainMenu
            case 1: return wifiMenu
            default: return "nie wybrano menu"
        }
    }

    const processConnect = function (str) {
        if(str == "-1") {
            tmpObj = {}
            pwd = 1
            return "Przerwano\n"
        }

        if(tmpObj.process == 1){
            tmpObj.process = 2
            let index = parseInt(str)
            tmpObj.index = index
            return "Wybrano siec: '" + tmpObj.ssids[index - 1].ssid + "'\nPodaj haslo:\n"
        }else if(tmpObj.process == 2){
            let ssid = tmpObj.ssids[tmpObj.index - 1].ssid
            tmpObj = {}
            pwd = 1
            return wifi.connect(ssid, str)
        }
        else{
            tmpObj = {}
            return "Nieokreslony stan\n"
        }
    }

    const bluetoothLog = function(str) {
        console.log("Log: " + str)
    }

    const processPing = async function (str) {
        pwd = 1
        return await asyncCommand("ping -c 4 " + str)
    }

    const processTerminal = async function (str) {
        if(str == 'exit'){
            pwd = 0
            return "Zakonczono\n"
        }
        return await asyncCommand(str)
    }

    const processMainMenu = async function (fCode) {
        if(fCode == 1) {
            pwd = 1
            return wifiMenu
        }else if(fCode == 2) {
            testLed()
            return "Test uruchomiony\n"
        }else if (fCode == 3) {
            pwd = 4
            return "Uruchamianianie terminala. ('exit' - konczy)"
        }else if(fCode == 9) {
            setTimeout(() => {
                asyncCommand("reboot")
            }, 500)
            return "Ponowne uruchamianie...\n"
        }

        return "Echo: '" + fCode + "' nieznana komenda!\n"
    }

    const processWifiMenu = async function (fCode) {
        if(fCode == 1){
            return (await wifi.scan()).str
        } else if(fCode == 2) {
            return wifi.getNetworks()
        } else if(fCode == 3) {
            return wifi.getStatus()
        } else if(fCode == 4) {
            pwd = 2
            tmpObj = {}
            let scan = await wifi.scan()
            tmpObj.process = 1
            tmpObj.ssids = scan.ssids
            return "Rozpoczeto tworzenie polaczenia ('-1' przerywa proces)\nWybierz siec: \n" + scan.str
        } else if(fCode == 5) {
            pwd = 3
            return "Wprowadz adres: \n"
        } else if (fCode == 6) {
            return (await asyncCommand('hostname')) + "\n" + (await showIp())
        }else if(fCode == 9){
            pwd = 0
            return mainMenu
        }

        return "Echo: '" + fCode + "' nieznana komenda!\n"
    }

    const processInput = async function (fCode) {
        if(fCode == 0){
            return printMenu()
        }

        switch (pwd) {
            case 0: return processMainMenu(fCode)
            case 1: return processWifiMenu(fCode)
        }
    }

    const send = function(str){
        server.write(Buffer.from(str), function (err, bytesWritten) {
            if (err) {
                console.log('Error!');
            } else {
                console.log('Send ' + bytesWritten + ' to the client!');
            }
        });
    }

    server.on('data', async function(buffer) {
        let str = buffer.toString()
        bluetoothLog("Received from client: " + str)

        str = str.trim()

        let response = "unknown\n"
        try{
            if(pwd == 1 || pwd == 0) {
                let code = parseInt(str)
                response = await processInput(code)
            } else if( pwd == 2) {
                response = await processConnect(str)
            } else if( pwd == 3) {
                response = await processPing(str)
            } else if(pwd == 4){
                response = await processTerminal(str)
            }
        }catch(e){
            response = e
        }

        bluetoothLog("Prepared to send to the client" + response)
        send(response)
    });




    server.listen(function (clientAddress) {
        console.log('Client: ' + clientAddress + ' connected!');
        pwd = 0
        send(printMenu())
    }, function(error){
        console.error("Something wrong happened!:" + error);
    }, {uuid: UUID, channel: CHANNEL} );

}

