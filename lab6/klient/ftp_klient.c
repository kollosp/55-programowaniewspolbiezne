// Proces wysyla a potem odbiera komunikaty udp
// Wspolpracuje z udp_serw
// Kompilacja gcc udp_cli.c -o udp_cli -lrt
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#define BUFLEN 80
#define KROKI 10
#define PORT 9950
#define SRV_IP "127.0.0.1"

#define OPENR 1 // Otwarcie pliku do odczytu
#define OPENW 2 // Otwarcie pliku do zapisu
#define READ 3 // Odczyt fragmentu pliku
#define CLOSE 4 // Zamkniecie pliku
#define WRITE 5 // Zapis fragmentu pliku
#define OPENDIR 6 // Otworz zdalny katalog
#define READDIR 7 // Czytaj zdalny katalog
#define STOP 10 // Zatrzymanie serwera

#define SIZE 512 // bajtów.
typedef struct {
	int typ; // typ zlecenia
	int ile; // liczba bajtow
	int fh; // uchwyt pliku
	char buf[SIZE]; // bufor
} mms_t;

void blad(char *s) {
	perror(s);
	_exit(1);
}

void readRemoteFile(int s, int slen, int blen, struct sockaddr_in *adr_moj, struct sockaddr_in *adr_serw){
	mms_t msg;	
	int rec, snd;
	do { //Otwarcie pliku
		printf("Podaj nazwe pliku: ");
		gets(msg.buf);
		printf("echo: %s\n", msg.buf);
		msg.typ = OPENR;
		snd = sendto(s, &msg, blen, 0, adr_serw, slen);
		if(snd < 0) perror("sendto()");
		rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
		if(rec < 0) perror("recvfrom()");
	} while(msg.fh < 0);

	do { // Odczyt --------------------
		msg.typ = READ;
		msg.ile = SIZE;
		snd = sendto(s, &msg, blen, 0, adr_serw, slen);
		if(snd < 0) perror("sendto()");
		rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
		if(rec < 0) perror("recvfrom()");
		printf("Odebrano: %d bajtow\n",msg.ile);
		if(msg.ile > 0) printf("%s\n",msg.buf);
		
	} while(msg.ile == SIZE);

	msg.typ = CLOSE;
	snd = sendto(s, &msg, blen, 0, adr_serw, slen);
	if(snd < 0) perror("sendto()");
	rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
	if(rec < 0) perror("recvfrom()");
	printf("zAMKNIETO PLIK\n",msg.ile);
}



void writeRemoteFile(int s, int slen, int blen, struct sockaddr_in *adr_moj, struct sockaddr_in *adr_serw){
	mms_t msg;	
	int rec, snd;
	int fd;
	do { //Otwarcie pliku
		printf("Podaj nazwe pliku: ");
		gets(msg.buf);
		fd = open(msg.buf, O_RDONLY);
		if(fd < 0){
			perror("File error");
			return;		
		}
	
		printf("echo: %s\n", msg.buf);
		msg.typ = OPENW;
		snd = sendto(s, &msg, blen, 0, adr_serw, slen);
		if(snd < 0) perror("sendto()");
		rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
	} while(msg.fh < 0);

	int bytes;
	do { // Zapis --------------------
		msg.typ = WRITE;
		bytes = read(fd, msg.buf, SIZE);
		msg.ile = bytes;		
		snd = sendto(s, &msg, blen, 0, adr_serw, slen);
		if(snd < 0) perror("sendto()");
		rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
		if(rec < 0) perror("recvfrom()");
		printf("Odebrano: %d bajtow\n",msg.ile);
		if(msg.ile > 0) printf("%s\n",msg.buf);
		
	} while(bytes == SIZE);

	msg.typ = CLOSE;
	snd = sendto(s, &msg, blen, 0, adr_serw, slen);
	if(snd < 0) perror("sendto()");
	rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
	if(rec < 0) perror("recvfrom()");
	printf("zAMKNIETO PLIK\n",msg.ile);
}

void readRemoteDir(int s, int slen, int blen, struct sockaddr_in *adr_moj, struct sockaddr_in *adr_serw){
	mms_t msg;	
	int rec, snd;
	int fd;
	printf("Podaj nazwe katalogu: ");
	gets(msg.buf);

	msg.typ = READDIR;
	msg.ile = 1;
	snd = sendto(s, &msg, blen, 0, adr_serw, slen);

	if(snd < 0) perror("sendto()");
	
	do{	
		rec = recvfrom(s, &msg, blen, 0, adr_moj, &slen);
		printf("%s\n", msg.buf);
	}while(msg.ile != 0);
}


int main(int argc, char * argv[]) {

	if(argc < 2) {
		printf("Podaj adres ip serwera w argumencie\n");
		_exit(1);
	}

	mms_t msg;
	struct sockaddr_in adr_moj, adr_serw, adr_x;
	int s, i, slen=sizeof(adr_serw), snd, blen=sizeof(msg),rec;
	

	s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if(s < 0) {
		blad("socket");
	}

	printf("Gniazdko %d utworzone\n",s);
	memset((char *) &adr_serw, 0, sizeof(adr_serw));
	adr_serw.sin_family = AF_INET;
	adr_serw.sin_port = htons(PORT);
	
	if (inet_aton(argv[1], &adr_serw.sin_addr)==0) {
		fprintf(stderr, "inet_aton() failed\n");
		_exit(1);
	}

	printf("adres serwera: %s\n", argv[1]);
	int next;
	do{
		printf("Menu\n");
		printf("0. zakoncz\n");
		printf("1. odczyt pliku\n");
		printf("2. zapis pliku\n");		
		printf("3. wylistowanie zawartosci katalogu\n");
		printf("Wybor: ");
		char n[10];
		gets(n);
		next = atoi(n);


		switch(next){
		case 0: break;		
		case 1:
			readRemoteFile(s, slen, blen, &adr_moj, &adr_serw);
		break;
		case 2:
			writeRemoteFile(s, slen, blen, &adr_moj, &adr_serw);
		break;
		case 3:
			readRemoteDir(s, slen, blen, &adr_moj, &adr_serw);
		break;
		default: printf("Niepoprawna komenda\n"); break;
		}

	}while(next != 0);



	
	close(s);
	return 0;
}
