#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>


//compile: gcc procm3.cpp && gcc proc_pot.cpp -o proc_pot && ./a.out 1 1 5 1 

int main(int argc, char* argv[]){


	if(argc < 2){
		fprintf(stderr, "Za malo argumentow\n");
		exit(1);
	}

	int pid, status;
	
	//argv[0] - run directory
	//argv[1] - parent steps
	for(int i=2;i<argc;++i){
		
		//child
		if((pid = fork()) == 0){
			char proc[10];
			sprintf(proc, "%d", i);
			execl("./proc_pot", argv[0],proc, argv[i], NULL); 
			fprintf(stderr, "Error occured %d", errno);		

		}
	}

	//parent

	int steps = atoi(argv[1]);

	for(int i=1;i<=steps;++i){
		printf("Proces macierzysty krok %d\n", i);
		sleep(1);
	}

	for(int i=2;i<argc;++i){
		pid = wait(&status);
		printf("Proces %d (pid: %d) zakonczony\n", WEXITSTATUS(status), pid); 
	}

	

	return 0;
} 
