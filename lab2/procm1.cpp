#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

//compile: gcc procm1.cpp && ./a.out 0 2 2 2

int main(int argc, char* argv[]){


	if(argc < 2){
		fprintf(stderr, "Za malo argumentow");
		exit(1);
	}

	int pid, status;
	
	//argv[0] - run directory
	//argv[1] - parent steps
	for(int i=2;i<argc;++i){
		
		//child
		if((pid = fork()) == 0){
			
			int steps = atoi(argv[i]);
			for(int j=1;j<=steps;++j){
				printf("Proces %d krok %d\n", i, j);
				sleep(1);
			}

			exit(i);
		}
	}

	//parent

	int steps = atoi(argv[1]);

	for(int i=1;i<=steps;++i){
		printf("Proces macierzysty krok %d\n", i);
		sleep(1);
	}

	for(int i=2;i<argc;++i){
		pid = wait(&status);
		printf("Proces %d (pid: %d) zakonczony\n", WEXITSTATUS(status), pid); 
	}

	

	return 0;
} 
