#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

//compile: gcc proc_pot.cpp -o proc_pot && ./proc_pot 4 10


int main(int argc, char* argv[]){


	if(argc < 3){
		fprintf(stderr, "Za malo argumentow\n");
		
		for(int i=0;i<argc;++i){
			printf("%d. %s\n",i, argv[i]);
		}

		exit(1);
	}
	

	int steps = atoi(argv[2]);
	for(int j=1;j<=steps;++j){
		printf("Proces %s krok %d\n", argv[1], j);
		sleep(1);
	}

	exit(atoi(argv[1]));
	
	return 0;
} 
