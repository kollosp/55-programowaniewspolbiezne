#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

const int SIZE = 512;

int main(int argc, char *argv[]) {
	int fd,rd, readBytes;
	char buf[SIZE];
	if(argc < 2) return 0;
	fd = open(argv[1],O_RDONLY);

	if(fd<0) {
		perror("open");
		exit(0);
	}

	rd = open(argv[2],O_CREAT | O_WRONLY, 0666);


	if(rd < 0){
		perror("open");
		exit(0);
	}

	do {
		readBytes = read(fd,buf,SIZE);
		write(rd,buf,readBytes);

	} while(readBytes > 0);

	close(fd);
	close(rd);

	return 1;
}
