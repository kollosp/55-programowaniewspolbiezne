#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>

struct result {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
 int count; // Ile liczb w przedziale
};

//compile: gcc procm4.cpp && gcc proc_pot.cpp -o proc_pot && ./a.out 1 1 5 1 

//returned in milliseconds
double getTime() {
	struct timeval  tv;
	gettimeofday(&tv, NULL);

	return (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; 
}

int main(int argc, char* argv[]){

	double start = getTime();

	int file;
	file = open("wynik.bin", O_CREAT | O_TRUNC, 0666);
	close(file);
	if(file < 0){
		perror("open");
		exit(1);		
	}

	//close(file);

	if(argc < 4){
		fprintf(stderr, "Za malo argumentow\n");
		printf("Podaj argumenty w postaci: [liczba procesów] [dolna granica] [gorna granica]\n");
		exit(1);
	}

	int pid;
	int procLimit = atoi(argv[1]);
	int lowerLimit = atoi(argv[2]);
	int upperLimit = atoi(argv[3]);


	printf("Wyszukiwanie liczb pierwszych w przedziale [%d, %d] wykorzystujac %d procesow\n",
	 lowerLimit, upperLimit, procLimit); 

	int perProc = (upperLimit-lowerLimit)/procLimit;

	int localLowerLimit = lowerLimit;


	//argv[0] - run directory
	//argv[1] - parent steps
	for(int i=0;i<procLimit;++i){

		//child
		if((pid = fork()) == 0){
			char procu[100];
			char procl[100];
			int l = localLowerLimit;
			int u; 

			if(i == procLimit-1){
				u = upperLimit; 
			}else{
				u = localLowerLimit + perProc;
			}

			printf("Uruchamiania procesu potomnego w przedziale: [%d, %d]\n",l,u);

			sprintf(procu, "%d", u);
			sprintf(procl, "%d", l);

			execl("./findPrimary", "", procl, procu, NULL); 
			fprintf(stderr, "Error occured %d | %s %s\n", errno, procu, procl);
			//	fprintf(stderr, "Error occured %d", errno);		
			exit(0);
		}

		localLowerLimit += perProc+1;
	}

	//parent

	int primaries = 0;
	for(int i=0;i<procLimit;++i){
		int status;
		pid = wait(&status);
		//printf("Proces (pid: %d) zakonczony %d\n", pid, status); 
	}

	//wczytanie struktur
	file = open("wynik.bin", O_RDONLY);
	if(file < 0){
		perror("open");
		exit(1);		
	}
	
	int readBytes = 0;
	struct result r;
	for(int i=0;i<procLimit;++i){
		readBytes += read(file, &r, sizeof(r)); 
		printf("Odczytano strukture nr %d, przedzial: [%d,%d], l.p. %d\n", i+1, r.begin, r.end, r.count);
		primaries+=r.count;
	}	

	close(file);


	double end = getTime();

	printf("Liczb pierwszych w przedziale [%d, %d] jest %d.\n Odnalezionow w czasie: %f, przez %d procesow\n",
	 lowerLimit, upperLimit, primaries, (end-start)/1000, procLimit); 


	return 0;
} 
