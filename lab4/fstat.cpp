#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>       
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char*argv[]){

	if(argc < 2){
		fprintf(stderr, "Za malo argumentow\n");
		
		for(int i=0;i<argc;++i){
			printf("%d. %s\n",i, argv[i]);
		}
	}

	int file;
	if ((file = open(argv[1], O_RDWR)) == -1) {
		perror("open");
		exit(0);
	}

	struct stat fileStat;

	int res = fstat(file,&fileStat);

	printf("size: %ldb\n", fileStat.st_size);
	printf("inode: %ld\n", fileStat.st_ino);
	printf("hard links: %ld\n", fileStat.st_nlink);
	printf("blocks [512b]: %ld\n", fileStat.st_blocks);
	printf("permissions:");
	
	printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
	printf( (S_IRUSR & fileStat.st_mode) ? "r" : "-");
	printf( (S_IWUSR & fileStat.st_mode) ? "w" : "-");
	printf( (S_IXUSR & fileStat.st_mode) ? "x" : "-");
	printf( (S_IRGRP & fileStat.st_mode) ? "r" : "-");	
	printf( (S_IWGRP & fileStat.st_mode) ? "w" : "-");	
	printf( (S_IXGRP & fileStat.st_mode) ? "x" : "-");	
	printf( (S_IROTH & fileStat.st_mode) ? "r" : "-");	
	printf( (S_IWOTH & fileStat.st_mode) ? "w" : "-");	
	printf( (S_IXOTH & fileStat.st_mode) ? "x" : "-");	
	printf("\n");	

	return 0;

}

