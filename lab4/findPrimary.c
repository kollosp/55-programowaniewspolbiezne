#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

struct result {
 int begin;// początek przedzialu
 int end; // koniec przedzialu
 int count; // Ile liczb w przedziale
};


int isPrimary(int n)
{ 
	int i,j=0;

	if(n <= 1) return 0;

	for(i=2;i*i<=n;i++) {
		if(n%i == 0) 
			return(0);	
	}
	return(1);
}

int lock(int file){
	int res = lockf(file,F_ULOCK,0);

	if (res == -1) {
		perror("lockf - 2");
	}
	return res;
}

int unlock(int file){
	int res = lockf(file,F_ULOCK,0);

	if (res == -1) {
		perror("lockf - 2");
	}
	return res;
}

int fileopen(){
	int file;

	file = open("wynik.bin", O_APPEND | O_WRONLY);	
	if(file < 0){
		perror("open");
		exit(1);		
	}

	return file;
}


int main(int argc, char*argv[]){

	if(argc < 3){
		fprintf(stderr, "Za malo argumentow\n");
		
		for(int i=0;i<argc;++i){
			printf("%d. %s\n",i, argv[i]);
		}

		printf("findPrimary: Wprowadz argumenty w postaci: [Granica dolna] [Granica górna]\n");

		exit(0);
	}

	int lowerLimit = atoi(argv[1]);
	int upperLimit = atoi(argv[2]);



	printf("findPrimary: [%d, %d]: Start\n", lowerLimit, upperLimit);

	if(lowerLimit >= upperLimit){
		fprintf(stderr, "Dolna granica wieksza od gornej\n");
		exit(0);
	}

	int primaries = 0;
	for(int i=lowerLimit;i<=upperLimit;++i){
		primaries += isPrimary(i);
	}


	printf("findPrimary: [%d, %d]: Zakonczono wyszukiwanie oczekiwanie na zapis\n", lowerLimit, upperLimit);

	

	struct result r;
	r.begin = lowerLimit;
	r.end = upperLimit;
	r.count = primaries;

	int file = fileopen();	
	if(lock(file) == -1) exit(1);

	printf("findPrimary: [%d, %d]: Ustawiono blokade\n", lowerLimit, upperLimit);
		
	int writtencount = write(file, &r, sizeof(r));
	
	if(writtencount < 0){
		perror("open");
		exit(1);		
	}

	if(unlock(file) == -1) exit(1);
	close(file);

	printf("findPrimary: [%d, %d]: Zwolniono blokade\n", lowerLimit, upperLimit);
	
	printf("findPrimary: odnaleziono: %d w przedziale [%d,%d]. Zapisano %d bytow\n",primaries, lowerLimit, upperLimit, writtencount);
	exit(0);
}

