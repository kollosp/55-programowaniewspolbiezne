#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <errno.h>


int isPrimary(int n)
{ 
	int i,j=0;

	if(n <= 1) return 0;

	for(i=2;i*i<=n;i++) {
		if(n%i == 0) 
			return(0);	
	}
	return(1);
}

int main(int argc, char*argv[]){

	if(argc < 3){
		fprintf(stderr, "Za malo argumentow\n");
		
		for(int i=0;i<argc;++i){
			printf("%d. %s\n",i, argv[i]);
		}

		printf("Wprowadz argumenty w postaci: [Granica dolna] [Granica górna]");

		exit(0);
	}

	int lowerLimit = atoi(argv[1]);
	int upperLimit = atoi(argv[2]);

	if(lowerLimit >= upperLimit){
		fprintf(stderr, "Dolna granica wieksza od gornej\n");
		exit(0);
	}

	int primaries = 0;
	for(int i=lowerLimit;i<=upperLimit;++i){
		primaries += isPrimary(i);
	}



	//printf("findPrimary: odnaleziono: %d w przedziale [%d,%d]\n",primaries, lowerLimit, upperLimit);
	exit(primaries);
}

